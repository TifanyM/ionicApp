import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SENTENCES } from '../../shared/sentences';
import { TextToSpeech } from '@ionic-native/text-to-speech';
import { SentencesProvider } from '../../providers/sentences/sentences';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  sentences ;//= SENTENCES;
  freeText;
  constructor(public navCtrl: NavController, private tts: TextToSpeech, private sentencesProvider: SentencesProvider) { 
    this.tts.speak({
      text: "Salut !",
      locale:"fr-FR",
      rate: 1.0
    })
    .then(() => console.log('Success'))
    .catch((reason: any) => console.log(reason));

    this.sentencesProvider.getSentences().then((sentences) => {
      console.log(sentences);
      this.sentences = sentences;
    }, (err) =>{
      console.log(err);
    })

  }

  onSubmit(txt){
    console.log(txt);
    this.tts.speak({
      text: txt,
      locale: "fr-FR",
      rate: 1.0
    })
    .then(() => console.log('Success'))
    .catch((reason: any) => console.log(reason));
  }


  
  

  
}
