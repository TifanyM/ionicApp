import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
// import { Observable } from 'rxjs';
/*
  Generated class for the SentencesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SentencesProvider {

  sentencesUrl = " http://localhost:3000/sentences";

  constructor(public http: HttpClient) {
    console.log('Hello SentencesProvider Provider');
  }

  getSentences() {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.sentencesUrl}`).subscribe(
        resp => {
          resolve(resp);
        },
        err => {
          reject(err);
        }
      );
    });
  }
}
